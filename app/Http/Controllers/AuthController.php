<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    
    public function login(Request $request) 
    {

        $username = $request->username;
        $password = $request->password;

        if (empty($username) or empty($password))
        {
            return response()->json([
                'status' => 'failed',
                'error' => 1,
                'message' => 'You must fill all the field'
            ]);
        }

        $credentials = request(['username', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);

    }

    public function register(Request $request) 
    {

        $username = $request->username;
        $password = $request->password;

        // Check if field is not empty
        if (empty($username) or empty($password))
        {
            return response()->json([ 'status' => 'failed', 'error' => 1, 'message' => 'You must fill all the field' ]);
        }

        // Check if email is valid
        if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return response()->json(['status' => 'failed', 'error' => 2, 'message' => 'You must enter a valid email']);
        }

        // Check if password is greater than 5 character
        if (strlen($password) < 6) {
            return response()->json(['status' => 'failed', 'error' => 3, 'message' => 'Password should be min 6 character']);
        }

        // Check if user already exist
        if (User::where('username', '=', $username)->exists()) {
            return response()->json(['status' => 'failed', 'error' => 4, 'message' => 'User already exists with this email']);
        }

        // Create new user
        try {
            $user = new User();

            $user->username = $username;
            $user->password = app('hash')->make($password);

            if ($user->save()) {
                return $this->login($request);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed', 'error' => 5, 'message' => $e->getMessage()]);
        }

    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

}
